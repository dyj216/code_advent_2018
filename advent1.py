with open("inputs/input1.txt") as f:
    frequencies = f.readlines()

frequencies = [int(a) for a in frequencies]
already_met_frequencies = set()
current_freq = 0
already_met_frequencies.add(current_freq)
repeated_freq = None
while repeated_freq is None:
    for freq in frequencies:
        current_freq += freq
        if current_freq not in already_met_frequencies:
            already_met_frequencies.add(current_freq)
        else:
            repeated_freq = current_freq
            break

print(repeated_freq)
