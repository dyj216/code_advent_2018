with open("inputs/input2.txt") as f:
    puzzle_inputs = [line.strip() for line in f.readlines()]

twos = 0
threes = 0
for line in puzzle_inputs:
    twos_found = False
    threes_found = False
    for c in line:
        if line.count(c) == 2 and not twos_found:
            twos += 1
            twos_found = True
        if line.count(c) == 3 and not threes_found:
            threes += 1
            threes_found = True
        if twos_found and threes_found:
            break


def find_common_characters(input_values):
    for i in range(len(input_values)):
        for j in range(i + 1, len(input_values)):
            differing_characters = []
            for k in range(len(input_values[i])):
                if input_values[i][k] != input_values[j][k]:
                    differing_characters.append(input_values[i][k])
                if len(differing_characters) > 1:
                    break
            if len(differing_characters) == 1:
                common_letters = list(input_values[i])
                common_letters.remove(differing_characters[0])
                return common_letters
    else:
        raise ValueError('input_values does not contain lines, which only differ in one character')


print(f'checksum: {twos * threes}, common characters: {"".join(find_common_characters(puzzle_inputs))}')
