with open("inputs/input3.txt") as f:
    inputs = f.readlines()

matrix = []
max_height = 0
max_width = 0
whole_cloth_id = 0
elf_params = []

for i in inputs:
    split_input = i.split()
    elf_id = int(split_input[0][1:])
    x, y = [int(z) - 1 if ':' not in z else int(z[:-1]) - 1 for z in split_input[2].split(',')]
    width, height = [int(z) for z in split_input[3].split('x')]
    elf_params.append({'id': elf_id, 'x': x, 'y': y, 'width': width, 'height': height})
    if max_width < x + width:
        max_width = x + width
    if max_height < y + height:
        max_height = y + height
    for line in matrix:
        while len(line) < max_width:
            line.append(0)
    while len(matrix) < max_height:
        matrix.append([0] * max_width)
    # print(f"elf_id: {elf_id}, x: {x}, y: {y}, width: {width}, height: {height}, {max_width}x{max_height}")
    for j in range(height):
        for k in range(width):
            line = matrix[y + j]
            line[x + k] += 1
    # break

overlapping = 0
not_overlapping = 0
for line in matrix:
    # print(line)
    zeros = line.count(0)
    ones = line.count(1)
    overlapping += (max_width - zeros - ones)
    not_overlapping += ones

for elf in elf_params:
    non_overlapping_size = 0
    for j in range(elf['height']):
        for k in range(elf['width']):
            line = matrix[elf['y'] + j]
            if line[elf['x'] + k] == 1:
                non_overlapping_size += 1
    if non_overlapping_size == elf['width'] * elf['height']:
        whole_cloth_id = elf['id']
        break

print(f'overlapping: {overlapping}, elf id: {whole_cloth_id}')
